package az.ingress.ms14.service;

import az.ingress.ms14.controller.Teacher;
import az.ingress.ms14.repository.TeacherRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {

    private final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public void update(Teacher teacher) {
        Optional<Teacher> entity=teacherRepository.findById(teacher.getId());
        entity.ifPresent(teacher1 -> {
            teacher1.setName(teacher.getName());
            teacher1.setBirthdate(teacher.getBirthdate());
            teacherRepository.save(teacher1);
        });
    }

    public void create(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    public Teacher get(Integer id) {
        return teacherRepository.findById(id).get();
    }

    public List<Teacher> getAll() {
        return teacherRepository.findAll();
    }

    public void delete(Integer id) {
        teacherRepository.deleteById(id);
    }

    public void deleteAll() {
        teacherRepository.deleteAll();
    }
}

