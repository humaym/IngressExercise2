package az.ingress.ms14.repository;

import az.ingress.ms14.controller.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher,Integer> {
}
