package az.ingress.ms14.controller;

import az.ingress.ms14.service.TeacherService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {


    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @PostMapping
    public void create(@RequestBody az.ingress.ms14.controller.Teacher teacher) {
        teacherService.create(teacher);
    }

    @PutMapping
    public void update(@RequestBody az.ingress.ms14.controller.Teacher teacher) {
        teacherService.update(teacher);
    }

    @GetMapping("/{id}")
    public az.ingress.ms14.controller.Teacher get(@PathVariable Integer id) {
        return teacherService.get(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        teacherService.delete(id);
    }


}
